import request from 'request';
import moment from 'moment';
import mysql from 'mysql';
import async from 'async';
import dbconfig from '../config/db-config';
import globalConfig from '../config/global_config';

const pool = mysql.createPool(dbconfig.dbConfig);

pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});


exports.startOptimization = (done) => {
    //get list campaigns by API
    request.get(globalConfig.campaignAPI.listCampaigns, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const campaigns = JSON.parse(body);
    
            if (campaigns.length > 0) {
                //parse campaigns statistics
                parseСampaignStats(campaigns, (result)=>{
                    return done(true);
                });                
            } else {
                console.log('No campaigns!');
            }
        } else {
            console.log(error);
        }
    });
}


exports.getLogsForCampaign = (reqBody, done) => {
    const campaignId = reqBody.campaign_id;   

    pool.getConnection(function (err, connection) {
        if (err) {
            return done(err);
        }
    
        //get logs for campaign
        let queryString = `SELECT * FROM log_update_bid WHERE campaign_id='${campaignId}'`;
        connection.query(queryString, function (err, rows, fields) {
            connection.release();
            if (err) {
                return done(err, null);
            }    
            return done(null, rows);
        });
    });
}


function safelyParseJSON(json) {
    return new Promise((resolve, reject) => {
        let parsed;
        try {
            parsed = JSON.parse(json)
        } catch (e) {
            reject(e);
        }
        resolve(parsed);
    })
};


async function parseСampaignStats(campaigns, done) {
    const allUsersInfo = [];
    const locsIds = [];
    for (let campaign of campaigns) {
        try {
            //get campaign statistics for apps
            let campaignStatisticsApps = await getCampaignStatisticsApps(campaign);

            //update bid for apps
            let udatedApps = await updateApps(campaign.id, campaignStatisticsApps);            
            console.log(`${udatedApps} campaign ${campaign.id}`);
        } catch (e) {
            console.log('Error: ', e);
        }
    }

    return done('Finished!');
}


function getCampaignStatisticsApps(campaign) {
    const campaignStatisticsUrl = `${globalConfig.campaignAPI.listCampaigns}/${campaign.id}/stats/apps`;

    return new Promise((resolve, reject) => {
        request.get(campaignStatisticsUrl, function (error, response, body) {
            if (error) {
                reject(error);
            }

            //parse body
            let bodyObject = safelyParseJSON(body).then(listApps => {
                if (listApps.meta) {
                    reject(listApps.meta);
                } else {
                    resolve(listApps);
                }
            }).catch(err => {
                reject(err);
            });
        });
    })
}


function updateApps(campaignId, campaignStatisticsApps) {    
    return new Promise((resolve, reject) => {
        let logUpdateData = [];
        async.each(campaignStatisticsApps, (appObj, callback) => {
            let impressOpportRatio = (appObj.impressions / appObj.opportunities).toFixed(1);
    
            // update bid if ratio < 0.5
            if (impressOpportRatio < 0.5) {
                let oldBid = 1;
                let newBid = 2;
                
                updateBID(campaignId, appObj, newBid, response => {
                    //push record for logs
                    let mysqlValues = `('${campaignId}', '${appObj.app_id}', '${oldBid}', '${newBid}' ,'${impressOpportRatio}', '${moment().utc().format('YYYY-MM-DD HH:mm:ss')}')`
                    logUpdateData.push(mysqlValues);

                    callback(null);
                });
            }else{
                callback(null);
            }
        }, err => {
            if (!err) {
                //save logs for apps
                writeLogUpdateBidToDB(logUpdateData);
                resolve('Updated');
            } else {
                reject(err);
            }
        });
    });
}


function updateBID(campaignId, appObj, newBid, done) {
    let updateBidURL = `${globalConfig.campaignAPI.listCampaigns}/update_bid`;

    request.put(updateBidURL, {
        form: {
            app_id: appObj.app_id,
            bid: newBid
        }
    }, function (error, response, body) {
        if (!error) {
            return done(null);
        } else {
            return done(error);
        }
    });
}


function writeLogUpdateBidToDB(mysqlValues) {
    const mysqlValuesString = mysqlValues.join(',');
    
    pool.getConnection(function (err, connection) {
        if (err) {
            return done(err);
        }
        let queryString = `INSERT INTO log_update_bid (campaign_id, app_id, old_bid, new_bid, ratio, created_at) VALUES ${mysqlValuesString}`;

        connection.query(queryString, function (err, rows, fields) {
            connection.release();
            if (err) {
                console.log(err);
                return err;
            }else{
                return 'Successfully saved!';
            }
        });
    });
};