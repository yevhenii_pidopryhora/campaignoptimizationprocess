import express from 'express';
import bodyParser from 'body-parser';
import campaignOptimizationProcess from './campaign_optimization_process';
import { error } from 'util';

const app = express();
const port = 5001;

app.use(bodyParser.json());
// tell the app to parse HTTP body messages
app.use(bodyParser.urlencoded({ extended: false }));


app.post('/campaigns_optimization', (req, res) => {
  campaignOptimizationProcess.startOptimization((result => {
    console.log('Finished!');
    res.send('Optimization finished!');
  }));
});


app.post('/get_logs_for_campaign', (req, res) => {
  campaignOptimizationProcess.getLogsForCampaign(req.body, (err, data) => {
    if (err) {
      res.send({
        status: false,
        error: err,
      });
    }
    res.send({
      status: false,
      data,
    });
  });
});


app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
