module.exports.dbConfig = {
    timeout: 60 * 60 * 1000,
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'campaigns_statistics',
    multipleStatements: true
};